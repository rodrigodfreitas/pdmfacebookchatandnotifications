package br.edu.ifpb.rfm.facebookappif;

import android.app.Activity;
import android.view.View.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LoginActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		Button button = (Button) findViewById(R.id.btLogin);
		button.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				Intent intent = new Intent(LoginActivity.this,
						NotificationActivity.class);
				LoginActivity.this.startActivity(intent);
			}
		});

	}
}
