package br.edu.ifpb.rfm.facebookappif;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class NewsActivity extends Activity {

	
	private TextView TitleView, ConteudoView, AutorView;
	private ImageView PhotoView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news);
		
		TitleView = (TextView)findViewById(R.id.txtTitle);
		ConteudoView = (TextView)findViewById(R.id.txtContent);
		AutorView = (TextView)findViewById(R.id.txtAuthor);
		PhotoView = (ImageView)findViewById(R.id.txtImage);
		
		Intent i= getIntent();
        Bundle bund = i.getExtras();

        if(bund!=null)
        {
            String j =(String) bund.get("titulo");
            TitleView.setText(j);
        
            String k =(String) bund.get("conteudo");
            ConteudoView.setText(k);

            String l =(String) bund.get("autor");
            AutorView.setText(l);
            
            Bitmap m =(Bitmap) bund.get("imagem");
            PhotoView.setImageBitmap(m);
        }
	}
}
